﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = "https://github.com/Martchus/syncthingtray/releases/download/v1.7.2/syncthingtray-qt5-1.7.2-i686-w64-mingw32.exe.zip"
$url64      = "https://github.com/Martchus/syncthingtray/releases/download/v1.7.2/syncthingtray-1.7.2-x86_64-w64-mingw32.exe.zip"
$checksum   = 'a0f3daae974d34599818fcd92202eebc5a3e3b1729112dbca2ed9d520033420a'
$checksum64 = 'b65db27d41ba6ea2449a0857e1d67d0c768b1dea4671ec365ef6a3b0e22e26a7'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64
  checksum      = $checksum
  checksumType  = 'sha256'
  checksum64    = $checksum64
  checksumType64= 'sha256'
}

Install-ChocolateyZipPackage @packageArgs
Move-Item -Force "$toolsDir\syncthingtray-*.exe" "$toolsDir\syncthingtray.exe"

$lnkPath = Join-Path ([Environment]::GetFolderPath('CommonPrograms')) 'Syncthing Tray.lnk'

Install-ChocolateyShortcut -ShortcutFilePath  $lnkPath -TargetPath "$toolsDir\syncthingtray.exe" -WorkingDirectory '%userprofile%'

# Restart syncthingtray if needed
if ($env:ChocolateySyncthingTrayNeedsRestart) {
  # The scheduler trick is needed to run a non-elevated process from an elevated shell
  # Inspired by https://github.com/chocolatey-community/chocolatey-packages/blob/master/automatic/spotify/tools/ChocolateyInstall.ps1

  # It doesn't matter what time we choose, we need to start it manually
  $Action = New-ScheduledTaskAction -Execute "$toolsDir\syncthingtray.exe" -WorkingDirectory "$toolsDir"
  $Time = New-ScheduledTaskTrigger -At (Get-Date -Format 'HH:mm') -Once
  Register-ScheduledTask -TaskName $packageArgs['packageName'] -Trigger $Time -Action $Action
  Start-ScheduledTask -TaskName $packageArgs['packageName']
  Start-Sleep -s 1
  Unregister-ScheduledTask -TaskName $packageArgs['packageName'] -Confirm:$False

  $env:ChocolateySyncthingTrayNeedsRestart = ''
}
