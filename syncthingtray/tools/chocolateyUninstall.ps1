$env:ChocolateySyncthingTrayNeedsRestart = ''

$lnkPath = Join-Path ([Environment]::GetFolderPath('CommonPrograms')) 'Syncthing Tray.lnk'

Remove-Item $lnkPath -ErrorAction SilentlyContinue -Force | Out-Null
