﻿$processName = 'syncthingtray*'
$process = Get-Process -Name $processName

if ($process) {
  Write-Host "Stopping Syncthing Tray process..."
  Stop-Process -InputObject $process

  Start-Sleep -Seconds 3

  $process = Get-Process -Name $processName
  if ($process) {
    Write-Warning "Killing Syncthing Tray process..."
    Stop-Process -InputObject $process -Force
  }

  Write-Warning "Syncthing Tray will be restarted if you are upgrading..."

  $toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
  $env:ChocolateySyncthingTrayNeedsRestart = '1'
}
