Import-Module chocolatey-au

. ..\common.ps1

$versionCheckInfo = @{
    OwnerAndRepo = 'Martchus/syncthingtray'
    VersionFromPattern = '^v'
    VersionToPattern = ''
    URL64Pattern = 'syncthingtray-[0-9.]+.*x86_64-w64-mingw32\.exe\.zip$'
    # Use Qt 5 as Qt 6 is no longer built for 32-bit:
    # https://github.com/Martchus/PKGBUILDs/commit/d73c79d876ca77338d758ebfd8f184db15be56fd
    URL32Pattern = 'syncthingtray-qt5-[0-9.]+.*i686-w64-mingw32\.exe\.zip$'
}

function global:au_SearchReplace {
    return ReplaceUrlAndChecksum
}

function global:au_GetLatest {
    return GetLatestFromGitHubLatestRelease @versionCheckInfo
}

update -ChecksumFor all
