Import-Module chocolatey-au

. ..\common.ps1

$versionCheckInfo = @{
    OwnerAndRepo = 'NLnetLabs/unbound'
    VersionFromPattern = 'release-([0-9.]+)'
    VersionToPattern = '$1'
}

function global:au_SearchReplace {
    return ReplaceUrlAndChecksum
}

function global:au_GetLatest {
    $Latest = GetLatestFromGitHubLatestRelease @versionCheckInfo

    $Latest['URL64'] = "https://www.nlnetlabs.nl/downloads/unbound/unbound_setup_$($Latest.Version).exe"
    $Latest['URL32'] = "https://www.nlnetlabs.nl/downloads/unbound/unbound_setup_$($Latest.Version)-w32.exe"

    return $Latest
}

update -ChecksumFor all
