﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$version    = '1.15.0'
$url        = "https://www.nlnetlabs.nl/downloads/unbound/unbound_setup_1.22.0-w32.exe"
$url64      = "https://www.nlnetlabs.nl/downloads/unbound/unbound_setup_1.22.0.exe"
$checksum   = 'b90a919f181fa4b64f51482d4c5d3d62912a42d73b6af2b4064569174d036bb7'
$checksum64 = '6084fb97325ba44ac84420d3b389faf073d99504ed8ce85cacbabfbb1a1fbbef'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64
  # Unbound uses NSIS https://github.com/NLnetLabs/unbound/blob/master/winrc/setup.nsi
  silentArgs    = "/S"
  softwareName  = 'Unbound*'
  checksum      = $checksum
  checksumType  = 'sha256'
  checksum64    = $checksum64
  checksumType64= 'sha256'
}

Install-ChocolateyPackage @packageArgs
