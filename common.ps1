function ReplaceUrlAndChecksum {
    $replaces = @{}

    if ($Latest.ContainsKey('URL32')) {
        $replaces += @{
            "(^[$]url\s*=\s*).*" = "`$1`"$($Latest.URL32)`""
            "(^[$]checksum\s*=\s*).*" = "`$1'$($Latest.Checksum32)'"
        }
    }
    if ($Latest.ContainsKey('URL64')) {
        $replaces += @{
            "(^[$]url64\s*=\s*).*" = "`$1`"$($Latest.URL64)`""
            "(^[$]checksum64\s*=\s*).*" = "`$1'$($Latest.Checksum64)'"
        }
    }

    return @{
        "tools\chocolateyInstall.ps1" = $replaces
    }
}

function GetLatestFromGitHubLatestRelease {
    param(
        [string]$OwnerAndRepo,
        [string]$VersionFromPattern,
        [string]$VersionToPattern,
        [string]$URL64Pattern,
        [string]$URL32Pattern
    )

    $latest_release_url = "https://api.github.com/repos/${OwnerAndRepo}/releases/latest"
    $latest_release = Invoke-WebRequest -Uri $latest_release_url -UseBasicParsing | ConvertFrom-Json

    $version = $latest_release.tag_name -replace $VersionFromPattern, $VersionToPattern

    $Latest = @{
        Version = $version
    }

    foreach ($asset in $latest_release.assets) {
        if ($PSBoundParameters.ContainsKey('URL64Pattern') -And ($asset.name -match $URL64Pattern)) {
            $Latest['URL64'] = $asset.browser_download_url
        }
        if ($PSBoundParameters.ContainsKey('URL32Pattern') -And ($asset.name -match $URL32Pattern)) {
            $Latest['URL32'] = $asset.browser_download_url
        }
    }

    if ($PSBoundParameters.ContainsKey('URL64Pattern') -And !$Latest.ContainsKey('URL64')) {
        throw "Missing URL64"
    }
    if ($PSBoundParameters.ContainsKey('URL32Pattern') -And !$Latest.ContainsKey('URL32')) {
        throw "Missing URL32"
    }

    return $Latest
}
