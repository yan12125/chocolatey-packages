# AU Packages Template: https://github.com/majkinetor/au-packages-template

$Options = [ordered]@{
    Push = $true

    GitLab = @{
        # Here the token is a project access token, which works with any non-blank username
        # https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html
        User = "oauth2"
        API_Key = $env:gitlab_token
        PushURL = "https://gitlab.com/yan12125/chocolatey-packages/"
        Branch = "main"
    }
}

updateall -Options $Options
