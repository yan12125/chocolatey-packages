Import-Module chocolatey-au

. ..\common.ps1

$versionCheckInfo = @{
    OwnerAndRepo = 'EasyIME/PIME'
    VersionFromPattern = 'v([0-9.]+)-stable'
    VersionToPattern = '$1'
    URL32Pattern = 'PIME-.*\.exe'
}

function global:au_SearchReplace {
    return ReplaceUrlAndChecksum
}

function global:au_GetLatest {
    return GetLatestFromGitHubLatestRelease @versionCheckInfo
}

update -ChecksumFor 32
