﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$version    = '1.3.0'
$url        = "https://github.com/EasyIME/PIME/releases/download/v$version-stable/PIME-$version-stable-setup.exe"
$checksum   = '6b2e288b95085a52f378f023fb17128f5b1389a3cdae078c7e4d47c1b4e9dc35'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  fileType      = 'exe'
  url           = $url
  # PIME uses NSIS https://github.com/EasyIME/PIME/blob/master/installer/installer.nsi
  silentArgs    = "/S"
  softwareName  = 'PIME*'
  checksum      = $checksum
  checksumType  = 'sha256'
}

Install-ChocolateyPackage @packageArgs
