Import-Module chocolatey-au

. ..\common.ps1

$versionCheckInfo = @{
    OwnerAndRepo = 'Tyrrrz/LightBulb'
    URL64Pattern = 'LightBulb-Installer\.win-x64\.exe$'
    URL32Pattern = 'LightBulb-Installer\.win-x86\.exe$'
}

function global:au_SearchReplace {
    return ReplaceUrlAndChecksum
}

function global:au_GetLatest {
    return GetLatestFromGitHubLatestRelease @versionCheckInfo
}

update -ChecksumFor all
