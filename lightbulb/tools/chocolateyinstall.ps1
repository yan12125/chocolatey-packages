﻿$ErrorActionPreference = 'Stop'
$packageName = $env:ChocolateyPackageName
$installDirPath = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = "https://github.com/Tyrrrz/LightBulb/releases/download/2.6/LightBulb-Installer.win-x86.exe"
$url64      = "https://github.com/Tyrrrz/LightBulb/releases/download/2.6/LightBulb-Installer.win-x64.exe"
$checksum   = '5fee710dd63900fe2c0691c3bdbe20ae0d25ecfe42e87a5bb043e41845cf3a5c'
$checksum64 = 'daa19d6609e9d9ec2ea0520b4f13247b2c44ea7b73ab503a04c1b409a5e98fd4'

# Install package
$packageArgs = @{
    packageName   = $packageName
    unzipLocation = $installDirPath
    fileType      = 'exe'
    url           = $url
    url64bit      = $url64
    # LightBulb provides ARM64 installers, while Chocolatey does not support ARM64 yet
    # https://github.com/chocolatey/choco/issues/1803
    softwareName  = 'lightbulb*'
    silentArgs    = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-'
    checksum      = $checksum
    checksumType  = 'sha256'
    checksum64    = $checksum64
    checksumType64= 'sha256'
}

Install-ChocolateyPackage @packageArgs

# Mark the executable as GUI
New-Item (Join-Path $installDirPath "LightBulb.exe.gui") -ItemType File -Force
